## Vagrant
* if using unix-like uncomment nfs *(line 62)*
    * more info [link](https://github.com/fideloper/Vaprobash)

## Laravel
* install laravel at `/vagrant/laravel`
    * `$laravel new laravel` at */vagrant*
    * more info [link](http://laravel.com/docs/installation#install-laravel)
    
## Dev Environment Vars

* create an `.env.{your_environment}.php` file
    * to get the current environment `$php artisan env`

## Autoloading (psr-4)
* screencast [link](https://laracasts.com/lessons/psr-4-autoloading)
* info [link](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader.md)
