<?php

Route::get('/test', function(){
    return "test";
});

Route::get('/', function(){
    return "landing";
});

// Crear Actividad
Route::post('/actividad/nueva', [
    'as'    => 'activity.new',
    'uses'  => 'ActivitiesController@newActivity']);

// Autentificar
Route::post('/login', [
    'as'    => 'login.post',
    'uses'  => 'AuthController@LogIn']);

// Asignar Password a Cliente
Route::post('/contraseña/nueva', [
    'as'    => 'password.new',
    'uses'  => 'AuthController@setPassword']);

// Oficializar Cotización
Route::post('/cotizacion/{cotizacion_id}/oficializar', [
    'as'    => 'coti.oficial',
    'uses'  => 'CotizacionController@oficializar']);

// Cliente Rechaza Cotización
Route::post('/cotizacion/{cotizacion_id}/rechazar', [
    'as'    => 'coti.reject',
    'uses'  => 'CotizacionController@reject']);

// Enviar Cotización a Cliente
Route::post('/cotizacion/enviar', [
    'as'    => 'coti.send',
    'uses'  => 'CotizacionController@send']);

// Cliente Abre Cotización
Route::get('/cotizacion/{cotizacion_id}', [
    'as'    => 'coti.show',
    'uses'  => 'CotizacionController@show']);

// Crear observación
Route::post('/observacion/nueva', [
    'as'    => 'comment.new',
    'uses'  => 'CommentController@new']);

// Enviar Solicitud a Empresa
Route::post('/solicitud/enviar',[
    'as'    => 'request.send',
    'uses'  => 'RequestsController@send']);

// Obtener Solicitudes
Route::get('/{user}/solicitudes',[
    'as'    => 'requests.get',
    'uses'  => 'RequestsController@getAll']);




